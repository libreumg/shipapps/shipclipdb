begin;
  select * from config.v_version;
  drop view if exists config.v_version;

  drop table config.t_menurole;
  drop type config.enum_keycloakrealmrole;

  \i views/version.sql
  select * from config.v_version;
commit;