begin;
  select * from config.v_version;
  drop view if exists config.v_version;

  alter table config.t_menuitem add column icon_url text;
  alter table config.t_menuitem add column order_nr int;
  update config.t_menuitem set order_nr = pk * 10;
  alter table config.t_menuitem alter column order_nr set not null;
	
  \i views/version.sql
  select * from config.v_version;
commit;