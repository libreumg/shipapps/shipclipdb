create table config.t_translation (
  pk int primary key generated always as identity,
  langkey text not null,
  localecode config.enum_locale not null,
  value text not null,
  unique(langkey, localecode)
);
