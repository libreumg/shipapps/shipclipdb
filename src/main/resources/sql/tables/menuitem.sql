create table config.t_menuitem(
  pk int primary key generated always as identity,
  url text not null unique,
  icon_url text,
  fk_label int not null references config.t_displaylabel(pk),
  fk_description int references config.t_displaylabel(pk),
  app_id text not null unique,
  order_nr int not null unique,
  keycloak_role text
);
