create table config.t_displaylabel (
  pk int primary key generated always as identity,
  label text,
  langkey text
);
