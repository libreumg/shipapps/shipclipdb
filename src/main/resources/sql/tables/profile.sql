create table config.t_profile (
  pk int primary key generated always as identity,
  keycloak_id text not null unique,
  theme text default 'light'
);
