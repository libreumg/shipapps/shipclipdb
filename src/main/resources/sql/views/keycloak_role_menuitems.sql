create view config.v_keycloak_role_menuitems as
select keycloak_role, string_agg(app_id, ', ') as app_ids
from config.t_menuitem
group by keycloak_role;
