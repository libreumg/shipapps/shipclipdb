begin;
  select * from config.v_version;
  drop view if exists config.v_version;

  \i enums/keycloakrealmrole.sql
  \i tables/menurole.sql
	
  \i views/version.sql
  select * from config.v_version;
commit;