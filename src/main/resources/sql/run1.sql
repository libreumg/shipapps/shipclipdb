begin;
  create schema config;
  -- select * from config.v_version;
  drop view if exists config.v_version;
  
  \i tables/displaylabel.sql
  \i tables/menuitem.sql
  
  \i views/version.sql
  select * from config.v_version;
commit;