begin;
  select * from config.v_version;
  drop view if exists config.v_version;

	\i tables/profile.sql
  
  \i views/version.sql
  select * from config.v_version;
commit;
