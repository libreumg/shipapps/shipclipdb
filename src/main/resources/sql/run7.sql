begin;
  select * from config.v_version;
  drop view if exists config.v_version;

  alter table config.t_menuitem add column keycloak_role text;
  update config.t_menuitem set keycloak_role = app_id; -- for the moment, use app_id as role
  \i views/keycloak_role_menuitems.sql
  
  select * from config.v_keycloak_role_menuitems;
  
  \i views/version.sql
  select * from config.v_version;
commit;
