create schema config;

\i enums/locale.sql

\i tables/displaylabel.sql
\i tables/menuitem.sql
\i tables/translation.sql
\i tables/profile.sql

\i views/keycloak_role_menuitems.sql
\i views/version.sql
