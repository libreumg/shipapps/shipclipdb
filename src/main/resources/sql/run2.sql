begin;
  select * from config.v_version;
  drop view if exists config.v_version;

	alter table config.t_menuitem add column app_id text unique;
  update config.t_menuitem set app_id = md5(url);
  alter table config.t_menuitem alter column app_id set not null;
  
  \i views/version.sql
  select * from config.v_version;
commit;