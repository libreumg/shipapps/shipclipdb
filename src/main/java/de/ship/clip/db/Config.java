/*
 * This file is generated by jOOQ.
 */
package de.ship.clip.db;


import de.ship.clip.db.tables.TDisplaylabel;
import de.ship.clip.db.tables.TMenuitem;
import de.ship.clip.db.tables.TProfile;
import de.ship.clip.db.tables.TTranslation;

import java.util.Arrays;
import java.util.List;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Config extends SchemaImpl {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>config</code>
     */
    public static final Config CONFIG = new Config();

    /**
     * The table <code>config.t_displaylabel</code>.
     */
    public final TDisplaylabel T_DISPLAYLABEL = TDisplaylabel.T_DISPLAYLABEL;

    /**
     * The table <code>config.t_menuitem</code>.
     */
    public final TMenuitem T_MENUITEM = TMenuitem.T_MENUITEM;

    /**
     * The table <code>config.t_profile</code>.
     */
    public final TProfile T_PROFILE = TProfile.T_PROFILE;

    /**
     * The table <code>config.t_translation</code>.
     */
    public final TTranslation T_TRANSLATION = TTranslation.T_TRANSLATION;

    /**
     * No further instances allowed
     */
    private Config() {
        super("config", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        return Arrays.asList(
            TDisplaylabel.T_DISPLAYLABEL,
            TMenuitem.T_MENUITEM,
            TProfile.T_PROFILE,
            TTranslation.T_TRANSLATION
        );
    }
}
